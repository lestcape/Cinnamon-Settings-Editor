#!/usr/bin/env python
# -*- coding:utf-8 -*-
#
# Cinnamon Installer
# Original source from: https://github.com/linuxmint/Cinnamon/blob/master/files/usr/lib/cinnamon-desktop-editor/cinnamon-desktop-editor.py
# Author: Lester Carballo Pérez <lestcape@gmail.com>
#
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 2 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
#  USA

from __future__ import print_function

import sys, os, stat, gettext, glob, shutil
from gi.repository import GLib, Gtk, Gdk, Gio, GObject, GdkPixbuf, CMenu

try:
   import json
except ImportError:
   import simplejson as json

# i18n
gettext.install("cinnamon", "/usr/share/locale")

ABS_PATH = os.path.abspath(__file__)
DIR_PATH = os.path.dirname(ABS_PATH)
CINNAMON_SETTINGS = "/usr/lib/cinnamon-settings"
HOME_PATH = os.path.expanduser("~")
UI_FILE = os.path.join(DIR_PATH, "cinnamon-settings-editor.ui")
PYTHON_HEAD = "#!/usr/bin/env python"

EXTENSIONS = (".png", ".xpm", ".svg")

class SurfaceWrapper(GObject.GObject):
    def __init__(self, surface):
        GObject.GObject.__init__(self)
        self.surface = surface

class IconPicker(object):
    def __init__(self, dialog, button, image):
        self.dialog = dialog
        self.button = button
        self.button.connect('clicked', self.pick_icon)
        self.image = image

    def pick_icon(self, button):
        chooser = Gtk.FileChooserDialog(title=_("Choose an icon"),
                                        parent=self.dialog,
                                        buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.REJECT,
                                        Gtk.STOCK_OK, Gtk.ResponseType.ACCEPT))
        chooser.add_shortcut_folder("/usr/share/pixmaps")
        chooser.add_shortcut_folder("/usr/share/icons")

        fn = self.get_icon_string(self.image)
        if fn is not None:
            if GLib.path_is_absolute(fn):
                chooser.set_filename(fn)
            else:
                theme = Gtk.IconTheme.get_default()
                icon_info = theme.lookup_icon(fn, 64, 0)
                icon_info_fn = icon_info.get_filename() if icon_info != None else None
                if icon_info_fn:
                    chooser.set_filename(icon_info_fn)
        filter = Gtk.FileFilter();
        filter.add_pixbuf_formats ();
        chooser.set_filter(filter);

        preview = Gtk.Image()
        chooser.set_preview_widget(preview)
        chooser.connect("update-preview", self.update_icon_preview_cb, preview)

        response = chooser.run()
        if response == Gtk.ResponseType.ACCEPT:
            self.set_icon_string (self.image, chooser.get_filename())
        chooser.destroy()

    def update_icon_preview_cb(self, chooser, preview):
        filename = chooser.get_preview_filename()
        if filename is None:
            return
        chooser.set_preview_widget_active(False)
        if os.path.isfile(filename):
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(filename, 128, 128)
            if pixbuf is not None:
                preview.set_from_pixbuf(pixbuf)
                chooser.set_preview_widget_active(True)

    def get_icon_string(self, image):
        # Detect if the user picked an icon, and make
        # it into an icon name.
        filename = image._file
        if filename is not None:
            if not filename.endswith(EXTENSIONS):
                return filename

            noext_filename = filename[:-4]

            theme = Gtk.IconTheme.get_default()
            resolved_path = None
            for path in theme.get_search_path():
                if noext_filename.startswith(path):
                    resolved_path = noext_filename[len(path):].lstrip(os.sep)
                    break

            if resolved_path is None:
                return filename

            parts = resolved_path.split(os.sep)
            # icon-theme/size/category/icon
            if len(parts) != 4:
                return filename

            return parts[3]

        return image._icon_name

    def set_icon_string(self, image, icon):
        if GLib.path_is_absolute(icon):
            image._file = icon
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size(icon, 64, 64)
            if pixbuf is not None:
                image.set_from_pixbuf(pixbuf)
        else:
            image._icon_name = self._strip_extensions(icon)
            image.set_from_icon_name (self._strip_extensions (icon), Gtk.IconSize.BUTTON)

    def _strip_extensions(self, icon):
        if icon.endswith(EXTENSIONS):
            return icon[:-4]
        else:
            return icon

class Main(object):
    def __init__(self, item_path = "", destdir = None):
        self._build_ui()

        self.item_path = item_path
        self.load_modules()
        self._resync_validity()

        self.tree = CMenu.Tree.new("cinnamon-applications.menu", CMenu.TreeFlags.INCLUDE_NODISPLAY)
        if not self.tree.load_sync():
            raise ValueError("Can not load menu tree")

        self.dialog.show_all()

    def set_check(self, ctl, name):
        if self.desk_app:
            try:
                val = self.desk_app.get_boolean(name)
            except GLib.GError:
                pass
            else:
                self.builder.get_object(ctl).set_active(val)

    def run(self):
        self.dialog.present()

    def load_module(self):
        self.builder.get_object('origin-entry').set_text(self.desk_app.get_filename())
        self.builder.get_object('name-entry').set_text(self.desk_app.get_display_name())
        self.builder.get_object('exec-entry').set_text(self.desk_app.get_commandline())
        self.builder.get_object('comment-entry').set_text(self.desk_app.get_description())
        self.builder.get_object('keywords-entry').set_text(self.get_cinnamon_keywords(self.desk_app))
        self.icon_picker.set_icon_string(self.builder.get_object('icon-image'), self.desk_app.get_icon().to_string())
        self.set_check('terminal-check', "Terminal")

    def load_modules(self):
        modules = { }
        if os.path.exists(self.destdir):
            f = open(self.destdir, "r")
            try:
                modules = json.load(f)
            except ValueError:
                pass
                try:
                    os.remove(self.destdir)
                except:
                    pass
        for origin in modules:
            module = modules[origin]
            wrapper = self._get_icon_wrapper(module["icon"])
            category = self._get_combo_value(module["category"])
            exec_text = module["command"].strip()
            if self._validate_exec_line(exec_text):
               state_icon = 'cs-xlet-running'
            else:
               state_icon = 'cs-xlet-error'
            iter = self.model.insert_before(None, None)
            self.model.set_value(iter, 0, module["origin"])
            self.model.set_value(iter, 1, category)
            self.model.set_value(iter, 2, module["name"])
            self.model.set_value(iter, 3, module["command"])
            self.model.set_value(iter, 4, module["keywords"])
            self.model.set_value(iter, 5, module["terminal"])
            self.model.set_value(iter, 6, module["comment"])
            self.model.set_value(iter, 7, module["icon"])
            self.model.set_value(iter, 8, state_icon)
            self.model.set_value(iter, 9, wrapper)

    def get_cinnamon_keywords(self, launcher):
        keywords = launcher.get_keywords()
        cinnamon_keywords = ""
        for key in keywords:
           cinnamon_keywords += key + ", "
        return cinnamon_keywords[:-2]

    def save_desktop_module(self):
        keyfile_edits = self._get_keyfile_edits()
        iter = self._get_row_iter(keyfile_edits["name"])
        if iter is not None:
            if not self._ask(_("This operation will update the selected items.\n\nDo you want to continue?")):
                return
        else:
            iter = self.model.insert_before(None, None)
        if keyfile_edits:
            self.model.set_value(iter, 0, keyfile_edits["origin"])
        else:
            self.model.set_value(iter, 0, "custtom")
        exec_text = keyfile_edits["command"].strip()
        if self._validate_exec_line(exec_text):
            state_icon = 'cs-xlet-running'
        else:
            state_icon = 'cs-xlet-error'
        self.model.set_value(iter, 1, self._get_combo_value(keyfile_edits["category"]))
        self.model.set_value(iter, 2, keyfile_edits["name"])
        self.model.set_value(iter, 3, keyfile_edits["command"])
        self.model.set_value(iter, 4, keyfile_edits["keywords"])
        self.model.set_value(iter, 5, keyfile_edits["terminal"])
        self.model.set_value(iter, 6, keyfile_edits["comment"])
        self.model.set_value(iter, 7, keyfile_edits["icon"])
        self.model.set_value(iter, 8, state_icon)
        self.model.set_value(iter, 9, keyfile_edits["wrapper"])
        
        launcher = self.desk_app
        name = launcher.get_display_name()
        icon = launcher.get_icon().to_string()
        command = launcher.get_commandline()
        comment = launcher.get_description()
        cinnamon_keywords = self.get_cinnamon_keywords(launcher)
        app_id = keyfile_edits["origin"]
        category = keyfile_edits["category"]
        try:
            name = name.decode('utf-8').encode('utf-8')
            icon = icon.decode('utf-8').encode('utf-8')
            command = command.decode('utf-8').encode('utf-8')
            comment = comment.decode('utf-8').encode('utf-8')
            cinnamon_keywords = cinnamon_keywords.decode('utf-8').encode('utf-8')
            app_id = app_id.decode('utf-8').encode('utf-8')
            category = category.decode('utf-8').encode('utf-8')
        except:
            pass
        # We can not use the laucher id here,
        # because will be imposible use 
        # the desktop file for tow commandline
        # option, but a python name with not english
        # text also is not a good solution.
        if name is not None:
            mod_name = name.replace(" ", "_").lower()

        if name == keyfile_edits["name"]:
            name = None
        if icon == keyfile_edits["icon"]:
            icon = None
        if command == keyfile_edits["command"]:
            command = None
        if comment == keyfile_edits["comment"]:
            comment = None
        if cinnamon_keywords == keyfile_edits["keywords"]:
            cinnamon_keywords = None

        self.save_module(mod_name, app_id, name, icon, command, comment, cinnamon_keywords, category)

    def save_module(self, mod_name, app_id, name, icon, command, comment, keywords, category):
        #if modules.find(mod_name) != -1:
        #    raise Exception("Invalid module name")
        module_filename = "cs_" + mod_name + ".py"
        module_path = os.path.join(CINNAMON_SETTINGS, "modules", module_filename)
        try:
            with open(module_path, 'w') as module_file:
                module_file.writelines(PYTHON_HEAD + '\n\n')
                module_file.writelines('from SettingsWidgets import *\n')
                module_file.writelines('from gi.repository import Gio\n')
                module_file.writelines('class Module:\n')
                module_file.writelines('    def __init__(self, content_box):\n')
                if (app_id is not None):
                    module_file.writelines('        launcher = Gio.DesktopAppInfo.new_from_filename("' + app_id + '")\n')
                if (keywords is None) and (app_id is not None):
                    module_file.writelines('        keywords = launcher.get_keywords()\n')
                    module_file.writelines('        cinnamon_keywords = ""\n')
                    module_file.writelines('        for key in keywords:\n')
                    module_file.writelines('           cinnamon_keywords += key + ", "\n')
                    module_file.writelines('        cinnamon_keywords = cinnamon_keywords[:-2]\n')
                elif (keywords is not None):
                    module_file.writelines('        keywords = "' + keywords + '\n')
                else:
                    raise Exception("Invalid keywords")
                if (name is None) and (app_id is not None):
                    module_file.writelines('        self.name = launcher.get_display_name()\n')
                elif (name is not None):
                    module_file.writelines('        self.name = "' + name + '"\n')
                else:
                    raise Exception("Invalid name")
                if (icon is None) and (app_id is not None):
                    module_file.writelines('        icon = launcher.get_icon().to_string()\n')
                elif (icon is not None):
                    module_file.writelines('        icon = "' + icon + '"\n')
                else:
                    raise Exception("Invalid icon")
                if (command is None) and (app_id is not None):
                    module_file.writelines('        command = launcher.get_commandline()\n')
                elif (command is not None):
                    module_file.writelines('        command = "' + command + '"\n')
                else:
                    raise Exception("Invalid command")
                module_file.writelines('        self.sidePage = SidePage(self.name, icon, keywords, content_box, exec_name=command, is_standalone=True, module=self)\n')
                if (comment is None) and (app_id is not None):
                    module_file.writelines('        self.comment = launcher.get_description()\n')
                elif (comment is not None):
                    module_file.writelines('        self.comment = "' + comment + '"\n')
                else:
                    raise Exception("Invalid comment")
                module_file.writelines('        self.category = "' + category + '"\n\n')
                module_file.writelines('    def on_module_selected(self):\n')
                module_file.writelines('        pass\n')
                st = os.stat(module_path)
                os.chmod(module_path, st.st_mode | stat.S_IRWXU | stat.S_IRWXG | stat.S_IROTH | stat.S_IXOTH | stat.S_IEXEC)
            self.save_modules()
        except IOError:
            pass

    def save_modules(self):
        modules = {}
        for row in self.model:
            origin = self.model.get_value(row.iter, 0)
            category = self._get_combo_id(self.model.get_value(row.iter, 1))
            modules[origin] = { "origin":     origin,
                                "category":   category,
                                "name":       self.model.get_value(row.iter, 2),
                                "command":    self.model.get_value(row.iter, 3),
                                "keywords":   self.model.get_value(row.iter, 4),
                                "terminal":   self.model.get_value(row.iter, 5),
                                "comment":    self.model.get_value(row.iter, 6),
                                "icon":       self.model.get_value(row.iter, 7)
                            }
        json_string = json.dumps(modules)
        with open(self.destdir, 'w') as f:
            f.write(json_string)

    def _on_pick_exec(self, button):
        chooser = Gtk.FileChooserDialog(title=_("Choose a command"),
                                        parent=self.dialog,
                                        buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.REJECT,
                                        Gtk.STOCK_OK, Gtk.ResponseType.ACCEPT))
        global_app = "/usr/share/applications"
        if os.path.exists(global_app):
            chooser.add_shortcut_folder(global_app)
        local_app = os.path.join(HOME_PATH, ".local/share/applications")
        if os.path.exists(local_app):
            chooser.add_shortcut_folder(local_app)
        if self.item_path and os.path.isfile(self.item_path):
            chooser.set_filename(self.item_path)
        elif os.path.exists(global_app):
            chooser.set_current_folder(global_app)
        response = chooser.run()
        if response == Gtk.ResponseType.ACCEPT:
            self._clear_all()
            self.item_path = self._escape_space(chooser.get_filename())
            self.desk_app = self._load_key_file(self.item_path)
            if self.desk_app:
                self.load_module()
            else:
                self.builder.get_object('origin-entry').set_text(self.item_path)
                self.builder.get_object('exec-entry').set_text(self.item_path)
                (fileName, fileExtension) = os.path.splitext(self.item_path)
                name = os.path.basename(fileName)
                self.builder.get_object('name-entry').set_text(name)
        chooser.destroy()

    def _on_response(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            self.save_desktop_module()
        else:
            self._end()
            self.dialog.destroy()

    def _on_selection_changed(self, selection):
        delete_button = self.builder.get_object('delete-button')
        test_button = self.builder.get_object('test-button')
        (model, pathlist) = selection.get_selected_rows()
        if len(pathlist) > 0:
            iter = self.model.get_iter(pathlist[0])
            # Select the current category
            self._select_combo_value(self.model.get_value(iter, 1))
            self.item_path = self.model.get_value(iter, 0)
            self.builder.get_object('origin-entry').set_text(self.item_path)
            self.builder.get_object('name-entry').set_text(self.model.get_value(iter, 2))
            self.builder.get_object('exec-entry').set_text(self.model.get_value(iter, 3))
            self.builder.get_object('comment-entry').set_text(self.model.get_value(iter, 6))
            self.builder.get_object('keywords-entry').set_text(self.model.get_value(iter, 4))
            self.icon_picker.set_icon_string(self.builder.get_object('icon-image'), self.model.get_value(iter, 7))
            self.builder.get_object('terminal-check').set_active(self.model.get_value(iter, 5) == "True")
            delete_button.set_sensitive(True)
            test_button.set_sensitive(True)
        else:
            delete_button.set_sensitive(False)
            test_button.set_sensitive(False)

    def _on_delete_button_clicked(self, button):
        tree_view = self.builder.get_object('module-list-treeview')
        selection = tree_view.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        iter = self.model.get_iter(pathlist[0])
        name = self.model.get_value(iter, 2)
        if len(pathlist) > 0 and self._ask(_("Are you sure you want to permanently delete %s?") % name):
            if name is not None:
                mod_name = name.replace(" ", "_").lower()
                module_filename = "cs_" + mod_name + ".py"
                module_path = os.path.join(CINNAMON_SETTINGS, "modules", module_filename)
                if os.path.isfile(module_path):
                    os.remove(module_path)
            self.model.remove(iter)
            self.save_modules()

    def _on_test_button_clicked(self, button):
        tree_view = self.builder.get_object('module-list-treeview')
        selection = tree_view.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        if len(pathlist) > 0:
            iter = self.model.get_iter(pathlist[0])
            command = self.model.get_value(iter, 3)
            os.system(command)

    def _clear_all(self):
        self.builder.get_object('origin-entry').set_text("")
        self.builder.get_object('name-entry').set_text("")
        self.builder.get_object('exec-entry').set_text("")
        self.builder.get_object('comment-entry').set_text("")
        self.builder.get_object('keywords-entry').set_text("")
        self.icon_picker.set_icon_string(self.builder.get_object('icon-image'), "gnome-panel-launcher")
        self.builder.get_object('terminal-check').set_active(False)
        self.builder.get_object('delete-button').set_sensitive(False)
        self.builder.get_object('test-button').set_sensitive(False)
        tree_view = self.builder.get_object('module-list-treeview')
        selection = tree_view.get_selection()
        selection.unselect_all()

    def _get_row_iter(self, name):
        iter = self.model.get_iter_first()
        while iter != None:
            curr_name = self.model.get_value(iter, 2)
            if curr_name == name:
                return iter
            iter = self.model.iter_next(iter)
        return None

    def _search_menu_sys(self, parent=None):
        if parent is None:
            parent = self.tree.get_root_directory()

        item_iter = parent.iter()
        item_type = item_iter.next()
        while item_type != CMenu.TreeItemType.INVALID:
            if item_type == CMenu.TreeItemType.DIRECTORY:
                item = item_iter.get_directory()
                self._search_menu_sys(item)
            elif item_type == CMenu.TreeItemType.ENTRY:
                item = item_iter.get_entry()
                if item.get_desktop_file_id() == self.desktop_file:
                    self.orig_file = item.get_desktop_file_path()
            item_type = item_iter.next()

    def _get_keyfile_edits(self):
        icon = self.icon_picker.get_icon_string(self.builder.get_object('icon-image'))
        wrapper = self._get_icon_wrapper(icon)
        [cat_id, cat_value] = self._get_current_combo_item()

        keyfile = { "origin"     : self.builder.get_object('origin-entry').get_text(),
                    "category"   : cat_id,
                    "name"       : self.builder.get_object('name-entry').get_text(),
                    "command"    : self.builder.get_object('exec-entry').get_text(),
                    "keywords"   : self.builder.get_object('keywords-entry').get_text(),
                    "terminal"   : self.builder.get_object('terminal-check').get_active(),
                    "icon"       : icon,
                    "comment"    : self.builder.get_object('comment-entry').get_text(),
                    "wrapper"    : wrapper
                  }
        return keyfile

    def _get_current_combo_item(self):
        combo = self.builder.get_object('category-combobox')
        iter = combo.get_active_iter()
        if iter != None:
            model = combo.get_model()
            return [model[iter][0], model[iter][1]]
        return ["", ""]

    def _get_combo_id(self, value):
        combo = self.builder.get_object('category-combobox')
        model = combo.get_model()
        for row in model:
            current = model.get_value(row.iter, 1)
            if current == value:
                return model.get_value(row.iter, 0)
        return ""

    def _get_combo_value(self, id):
        combo = self.builder.get_object('category-combobox')
        model = combo.get_model()
        for row in model:
            current = model.get_value(row.iter, 0)
            if current == id:
                return model.get_value(row.iter, 1)
        return ""

    def _select_combo_id(self, id):
        combo = self.builder.get_object('category-combobox')
        model = combo.get_model()
        for row in model:
            current = model.get_value(row.iter, 0)
            if current == id:
                combo.set_active_iter(row.iter)

    def _select_combo_value(self, value):
        combo = self.builder.get_object('category-combobox')
        model = combo.get_model()
        for row in model:
            current = model.get_value(row.iter, 1)
            if current == value:
                combo.set_active_iter(row.iter)

    def _load_key_file(self, path):
        try:
            return Gio.DesktopAppInfo.new_from_filename(path);
        except:
            pass
        return None;

    def _escape_space(self, string):
        return string.replace(" ", "\ ")

    def _get_icon_wrapper(self, icon):
        scale = self.dialog.get_scale_factor()
        img = None
        w = -1
        h = 30 * scale

        if not os.path.exists(icon):
            theme = Gtk.IconTheme.get_default()
            if theme.has_icon(icon):
                img = theme.load_icon(icon, h, 0)
        else:
            try:
                img = GdkPixbuf.Pixbuf.new_from_file_at_size(icon, w, h)
            except:
                theme = Gtk.IconTheme.get_default()
        surface = None
        if img is not None:
            surface = Gdk.cairo_surface_create_from_pixbuf (img, scale, self.dialog.get_window())
        wrapper = SurfaceWrapper(surface)
        return wrapper

    def _icon_data_func(self, column, cell, model, iter, data=None):
        wrapper = model.get_value(iter, 9)
        if wrapper:
            cell.set_property("surface", wrapper.surface)
        else:
            cell.set_property("surface", None)

    def _build_ui(self):
        self.builder = Gtk.Builder()
        self.builder.set_translation_domain('cinnamon')
        self.builder.add_from_file(UI_FILE)
        self.destdir = os.path.join(DIR_PATH, "cinnamon-settings-editor.json")
        self.dialog = self.builder.get_object('editor')
        self.dialog.connect('response', self._on_response)

        icon = self.builder.get_object('icon-image')
        icon._file = None
        icon._icon_name = None

        tree_view = self.builder.get_object('module-list-treeview')
        tree_view.get_selection().connect('changed', self._on_selection_changed)
        self.model = tree_view.get_model()

        delete_button = self.builder.get_object('delete-button')
        delete_button.connect('clicked', self._on_delete_button_clicked)
        delete_button.set_sensitive(False)

        test_button = self.builder.get_object('test-button')
        test_button.connect('clicked', self._on_test_button_clicked)
        test_button.set_sensitive(False)

        icon_column = self.builder.get_object('icon-tv-column')
        cr_icon = self.builder.get_object('icon-cr')
        icon_column.set_cell_data_func(cr_icon, self._icon_data_func)

        self.icon_picker = IconPicker(self.dialog,
                                      self.builder.get_object('icon-button'),
                                      self.builder.get_object('icon-image'))

        self.builder.get_object('origin-browse').connect('clicked', self._on_pick_exec)

        self.builder.get_object('name-entry').connect('changed', self._resync_validity)
        self.builder.get_object('exec-entry').connect('changed', self._resync_validity)

    def _sync_widgets(self, name_valid, name_override, exec_valid):
        if name_valid and name_override:
            self.builder.get_object('name-entry').set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, 'gtk-dialog-info')
            self.builder.get_object('name-entry').set_icon_tooltip_text(Gtk.EntryIconPosition.SECONDARY, _("Valid"))
        elif name_valid:
            self.builder.get_object('name-entry').set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, 'gtk-ok')
            self.builder.get_object('name-entry').set_icon_tooltip_text(Gtk.EntryIconPosition.SECONDARY, _("Valid"))                                                         
        else:
            self.builder.get_object('name-entry').set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, 'gtk-dialog-warning')
            self.builder.get_object('name-entry').set_icon_tooltip_text(Gtk.EntryIconPosition.SECONDARY,
                                                                        _("Name cannot be blank."))

        if exec_valid:
            self.builder.get_object('exec-entry').set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, 'gtk-ok')
            self.builder.get_object('exec-entry').set_icon_tooltip_text(Gtk.EntryIconPosition.SECONDARY,
                                                                        _("Valid"))
        else:
            self.builder.get_object('exec-entry').set_icon_from_icon_name(Gtk.EntryIconPosition.SECONDARY, 'gtk-dialog-warning')
            self.builder.get_object('exec-entry').set_icon_tooltip_text(Gtk.EntryIconPosition.SECONDARY,
                                                                        _("Cannot be empty.  Spaces in filenames must be escaped with backslash (\\).\nNot a valid executable line."))

        self.builder.get_object('ok').set_sensitive(name_valid and exec_valid)

    def _resync_validity(self, *args):
        name_text = self.builder.get_object('name-entry').get_text().strip()
        exec_text = self.builder.get_object('exec-entry').get_text().strip()
        name_valid = (name_text is not "")
        name_override = (self._get_row_iter(name_text) != None)
        exec_valid = self._validate_exec_line(exec_text)
        self._sync_widgets(name_valid, name_override, exec_valid)

    def _validate_exec_line(self, string):
        try:
            success, parsed = GLib.shell_parse_argv(string)
            if GLib.find_program_in_path(parsed[0]) or ((not os.path.isdir(parsed[0])) and os.access(parsed[0], os.X_OK)):
                if os.access(CINNAMON_SETTINGS, os.W_OK):
                    return True
        except:
            pass
        return False

    def _ask(self, msg):
        dialog = Gtk.MessageDialog(None,
                                   Gtk.DialogFlags.DESTROY_WITH_PARENT,
                                   Gtk.MessageType.QUESTION,
                                   Gtk.ButtonsType.YES_NO,
                                   None)
        dialog.set_default_size(400, 200)
        dialog.set_markup(msg)
        dialog.show_all()
        response = dialog.run()
        dialog.destroy()
        return response == Gtk.ResponseType.YES

    def _end(self):
        Gtk.main_quit()

if __name__ == "__main__":
    Gtk.Window.set_default_icon_name('gnome-panel-launcher')
    Main()
    Gtk.main()
