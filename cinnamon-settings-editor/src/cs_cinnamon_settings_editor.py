#!/usr/bin/env python

from SettingsWidgets import *
from gi.repository import Gio

import os

EDITOR_PATH = "/usr/lib/cinnamon-settings/bin/cinnamon_settings_editor.py"

class Module:
    def __init__(self, content_box):
        keywords = _("Settings") + ", Cinnamon"
        self.name = _("Cinnamon Settings Editor")
        if self._is_program_in_system("pkexec"):
            command =  "pkexec %s" % EDITOR_PATH
        elif self._is_program_in_system("gksudo"):
            message = _("The program %s is requesting elevated privileges to perform a change on your system.") % self.name
            message = message + "\n" +_("Enter the root password to allow this task.")
            command =  "gksudo %s" % (EDITOR_PATH)
        else:
            raise Exception("Can not find any GUI sudo programs")

        sidePage = SidePage(self.name, "gnome-settings", keywords, content_box, exec_name=command, is_standalone=True, module=self)
        self.sidePage = sidePage
        self.comment = _("Cinnamon Settings Editor")
        self.category = "admin"

    def _is_program_in_system(self, programName):
        path = os.getenv('PATH')
        for p in path.split(os.path.pathsep):
            p = os.path.join(p, programName)
            if os.path.exists(p) and os.access(p, os.X_OK):
                return True
        return False

    def on_module_selected(self):
        pass
